package ittdb;


import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import userinterface.EmployeeDetails;
import userinterface.StudentDetails;

import dbexception.ITTDBException;



public class ITTDBTest {
	static ITTDB db;
	static StudentDetails student1,student2,student3;
	static EmployeeDetails employee1,employee2,employee3;

	@BeforeClass
	public static void testDeclareStudent() {       
		db = new ITTDB();
		student1 = new StudentDetails("Garima", "Udaipur");		
	}
	@BeforeClass     
	public  static void testDeclareEmployee() {
		employee1 = new EmployeeDetails("Garima", "Jr.Software Engineer");
	}

	@Test(priority = 1)
	public void testsaveStudent() throws IOException, ITTDBException {          
		db.insert(student1); 
	}


	@Test(priority = 2)
	public void testsaveEmployee() throws IOException, ITTDBException {          
		db.insert(employee1); 
	}


	@Test(priority = 4)
	public void testsearch() throws IOException, ITTDBException {     
		EmployeeDetails ExpectedDetails = new EmployeeDetails();
		ExpectedDetails.setId(1);
		ExpectedDetails.setName("Rajveer");
		ExpectedDetails.setDesignation("Software Engineer");
		List<EmployeeDetails> employee = db.search(employee1, "id", "1");
		Assert.assertEquals(employee.get(0).toString(),ExpectedDetails.toString());
	}

	@Test(priority = 5)
	public void TestDelete() throws IOException, ITTDBException {
		int rowsAffected = db.delete(student1, "id", "1");
		Assert.assertEquals(1,rowsAffected);
	}

	@Test(priority = 3)
	public void testUpdate() throws IOException, ITTDBException {
		employee1.setName("Rajveer");
		employee1.setDesignation("Software Engineer");
		int rowsUpdated = db.update(employee1, 1);
		Assert.assertEquals(1,rowsUpdated);
	}
}
