package fileutil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
public class FileStorage {

	public static <T> String getFilePath(T object) {
		return System.getProperty("user.dir") + "/ITTDB-FileStore/" + object.getClass().getSimpleName() + ".json";
	}

	public static boolean isFileExist(String filePath) {
		return (Files.exists(Paths.get(filePath), LinkOption.NOFOLLOW_LINKS)) ? true : false;
	}

	public static File CreateFile(String filePath) {
		return new File(filePath);
	}

	public static boolean isFileEmpty(String filePath) throws IOException{
		boolean CheckEmpty = false;
		
		try(FileInputStream fileInputStream = new FileInputStream(filePath)) {			
			CheckEmpty = (fileInputStream.getChannel().size() == 0) ? true : false;
		}
		return CheckEmpty;
	}

}
