package userinterface;

public class StudentDetails {
	public int id;
	public String name;
	public String address;

	StudentDetails() {

	}

	public StudentDetails(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

    

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Student [Id = " + id + ", Name = " + name + ", Address = " + address + "]";
	}
}
