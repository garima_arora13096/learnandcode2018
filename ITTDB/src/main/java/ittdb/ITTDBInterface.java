package ittdb;


import java.io.IOException;
import java.util.List;

import dbexception.ITTDBException;

public interface ITTDBInterface {
	public <T> void insert(T object) throws IOException, ITTDBException;
	public <T> List<T> search(T object, String searchKey, String searchValue)
			throws IOException, ITTDBException;
	public <T> int delete(T object, String deleteKey, String deleteValue)
			throws IOException, ITTDBException;
	public <T> int update(T object, int id) throws IOException, ITTDBException;
}
