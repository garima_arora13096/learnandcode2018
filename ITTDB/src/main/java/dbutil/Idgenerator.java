package dbutil;
import java.util.List;
public class Idgenerator {
	public static <T> void generateId(List<T> objectList, T object) {
		int lastId;
		if (objectList.size() == 0) {
			DbStorage.setId(object, 1);
		} else {
			lastId = new Integer(DbStorage.getProperty(objectList.get(objectList.size()-1), "id"));
			DbStorage.setId(object, lastId + 1);
		}
	}	
}
