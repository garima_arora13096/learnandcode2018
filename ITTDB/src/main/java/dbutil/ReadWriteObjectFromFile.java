package dbutil;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ReadWriteObjectFromFile {
static ObjectMapper objectMapper = new ObjectMapper();
    
    public static <T> List<T> ReadObjectsFromFile(String filePath, Class<T> classType) throws IOException {
		List<T> ObjectList;
		try (FileInputStream fileInputStream = new FileInputStream(filePath)) {
                    JsonParser Parser = objectMapper.getFactory().createParser(fileInputStream);
			ObjectList = objectMapper.readValue(Parser, objectMapper.getTypeFactory().constructCollectionType(List.class, classType));
		}
		return ObjectList;
	}

	public static <T> void WriteObjectsToFile(String filePath, List<T> objectList) throws IOException {

		try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
                    JsonGenerator JSONGenerator = objectMapper.getFactory().createGenerator(outputStream);
			objectMapper.writerWithDefaultPrettyPrinter().writeValue(JSONGenerator, objectList);
		}
	}
}
