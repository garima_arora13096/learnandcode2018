/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserIinterface;

/**
 *
 * @author garima.arora
 */
public class StudentDetails {
	public int id;
	public String name;
	public String address;

	StudentDetails() {

	}

	public StudentDetails(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

    

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Student [Id = " + id + ", Name = " + name + ", Address = " + address + "]";
	}
}
