/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserIinterface;

/**
 *
 * @author garima.arora
 */

    public class EmployeeDetails {
	public int id;
	public String name;
	public String designation;

	public EmployeeDetails() {

	}

	public EmployeeDetails(String name, String designation) {
		super();
		this.name = name;
		this.designation = designation;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Employee [Id = " + id + ", Name = " + name + ", Designation = " + designation + "]";
	}
    
    }
