/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ittdbtest;

import ITTdbOperations.Exceptions.ITTDBException;
import java.io.IOException;

/**
 *
 * @author garima.arora
 */
public class Main extends DBTester {
    
    public static void main(String args[]) throws IOException, ITTDBException
    {
        DBTester testDB = new DBTester();
        testDB.DeclareStudent();
        testDB.DeclareEmployee();
        testDB.TestsaveStudent();
        testDB.TestsaveEmployee();
    }
}
