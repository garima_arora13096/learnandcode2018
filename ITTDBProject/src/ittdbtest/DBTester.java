/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ittdbtest;

/**
 *
 * @author garima.arora
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import ITTDB.ITTDB;
import ITTdbOperations.Exceptions.ITTDBException;
import UserIinterface.EmployeeDetails;
import UserIinterface.StudentDetails;
import java.io.IOException;
import java.util.List;
import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DBTester {    
ITTDB db;
static StudentDetails student1,student2,student3;
static EmployeeDetails employee1,employee2,employee3;

@BeforeClass

        public void DeclareStudent() {       
            db = new ITTDB();
            student1 = new StudentDetails("Garima", "Udaipur");		
        }
        
        public void DeclareEmployee() {
            employee1 = new EmployeeDetails("Garima", "Jr.Software Engineer");
        }
		
	@Test
        public void TestsaveStudent() throws IOException, ITTDBException {          
	db.insert(student1); 
        }
        
        @Test
        public void TestsaveEmployee() throws IOException, ITTDBException {          
	db.insert(employee1); 
        }
                
	@Test
        public void Testsearch() throws IOException, ITTDBException {     
        EmployeeDetails ExpectedDetails = new EmployeeDetails();
        ExpectedDetails.setId(1);
        ExpectedDetails.setName("Garima");
        ExpectedDetails.setDesignation("Jr.Software Engineer");
        List<EmployeeDetails> employee = db.search(employee1, "name", "Garima");
        Assert.assertEquals(employee.get(0).toString(),ExpectedDetails.toString());
        }

        @Test
        public void TestDelete() throws IOException, ITTDBException {
        int rowsAffected = db.delete(student1, "id", "1");
        Assert.assertEquals(1,rowsAffected);
        }

        @Test
        public void TesUpdate() throws IOException, ITTDBException {

        student1.setName("Rajveer");
        student1.setAddress("Udaipur");
        int rowsUpdated = db.update(student1, 1);
        Assert.assertEquals(1,rowsUpdated);
    }
}