/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITTdbOperations.Exceptions;

/**
 *
 * @author garima.arora
 */
public class ITTDBException extends Exception {
	String errorMessage;
	int errorCode;

	public ITTDBException(int errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public void printMessage() {
		System.out.println(errorCode + ":" + errorMessage);
	}
        
        
}

