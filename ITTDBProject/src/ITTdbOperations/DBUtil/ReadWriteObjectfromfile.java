/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITTdbOperations.DBUtil;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author garima.arora
 */
public class ReadWriteObjectfromfile {
     static ObjectMapper objectMapper = new ObjectMapper();
    
    public static <T> List<T> ReadObjectsFromFile(String filePath, Class<T> classType) throws IOException {
		List<T> ObjectList;
		try (FileInputStream fileInputStream = new FileInputStream(filePath)) {
                    JsonParser Parser = objectMapper.getFactory().createParser(fileInputStream);
			ObjectList = objectMapper.readValue(Parser, objectMapper.getTypeFactory().constructCollectionType(List.class, classType));
		}
		return ObjectList;
	}

	public static <T> void WriteObjectsToFile(String filePath, List<T> objectList) throws IOException {

		try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
                    JsonGenerator JSONGenerator = objectMapper.getFactory().createGenerator(outputStream);
			objectMapper.writerWithDefaultPrettyPrinter().writeValue(JSONGenerator, objectList);
		}
	}
}
