/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITTdbOperations.DBUtil;

import ITTdbOperations.Exceptions.ITTDBException;
import ITTdbOperations.FileUtil.FileStorage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author garima.arora
 */
public class DbStorage {
    
    public static <T> void insert(T object) throws IOException {
            
		List<T> objectList = new ArrayList<T>();
		String filePath = FileStorage.getFilePath(object);

		if (!FileStorage.isFileExist(filePath)) {
			FileStorage.CreateFile(filePath);
		} else if (!FileStorage.isFileEmpty(filePath)) {
			objectList = (List<T>) ReadWriteObjectfromfile.ReadObjectsFromFile(filePath, object.getClass());
		}
		Idgenerator.generateId(objectList, object);
		objectList = AddObjectsInObjectList(objectList, object);
		ReadWriteObjectfromfile.WriteObjectsToFile(filePath, objectList);
                
	}
        
	public static <T> List<T> search(String searchKey, String searchValue, T object)
			throws ITTDBException, IOException {

		String filePath = FileStorage.getFilePath(object);
		List<T> objectList = (List<T>) ReadWriteObjectfromfile.ReadObjectsFromFile(filePath, object.getClass());
		return getSearchedObjects(searchKey, searchValue, objectList);
	}

	public static <T> List<T> getSearchedObjects(String searchKey, String searchValue, List<T> objectList)
			throws ITTDBException {
		List<T> searchedObjects = new ArrayList<>();
		for (T tempObject : objectList) {
			if (getProperty(tempObject, searchKey).contains(searchValue)) {
				searchedObjects.add(tempObject);
			}
		}
		if (searchedObjects.isEmpty()) {
			throw new ITTDBException(007, "Object not found");
		}
		return searchedObjects;
	}

	public static <T> int delete(T object, String deleteKey, String deleteValue)
			throws IOException, ITTDBException {
		int rowsAffected = 0;
		String filePath = FileStorage.getFilePath(object);
		List<T> objectList = (List<T>) ReadWriteObjectfromfile.ReadObjectsFromFile(filePath, object.getClass());

		List<T> objectsToDelete = DbStorage.search(deleteKey, deleteValue, object);
		rowsAffected = remove(objectsToDelete.get(0), objectList);

		if (rowsAffected > 0)
			ReadWriteObjectfromfile.WriteObjectsToFile(filePath, objectList);
		return rowsAffected;
	}

	public static <T> int remove(T object, List<T> objectList) {
		int rowsAffected = 0;
		for (int index = 0; index < objectList.size(); index++) {
			T tempObject = objectList.get(index);
			if (getProperty(tempObject, "id").equals(getProperty(object, "id"))) {
				objectList.remove(index);
				rowsAffected += 1;
				break;
			}
		}
		return rowsAffected;
	}

	public static <T> int update(T object, int id) throws IOException, ITTDBException {
		int rowsAffected = 0;
		String filePath = FileStorage.getFilePath(object);
		List<T> objectList = (List<T>) ReadWriteObjectfromfile.ReadObjectsFromFile(filePath, object.getClass());
		rowsAffected = setUpdatedObjectInList(id, object, objectList);
		if (rowsAffected > 0)
			ReadWriteObjectfromfile.WriteObjectsToFile(filePath, objectList);
		return rowsAffected;
	}

	public static <T> int setUpdatedObjectInList(int id, T object, List<T> objectList) {
		int rowsAffected = 0;
		for (int index = 0; index < objectList.size(); index++) {
			T tempObject = objectList.get(index);
			if (getProperty(tempObject, "id").equals(new Integer(id).toString())) {
				setId(object, id);
				objectList.set(index, object);
				rowsAffected += 1;
				break;
			}
		}
		return rowsAffected;
	}

        public static <T> List<T> AddObjectsInObjectList(List<T> objectList, T object) {
		objectList.add(object);
		return objectList;
	}
        
         public static <T> String getProperty(T object, String property) {
		String value = " ";
		try {
			value = object.getClass().getDeclaredField(property).get(object).toString();
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
		}
		return value;
	}
	public static <T> void setId(T object, int id) {
		try {
			object.getClass().getDeclaredField("id").set(object, id);
                        
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
		}
	}
}
