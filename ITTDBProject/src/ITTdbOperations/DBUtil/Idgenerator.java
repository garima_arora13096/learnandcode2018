/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITTdbOperations.DBUtil;

import java.util.List;
/**
 *
 * @author garima.arora
 */
public class Idgenerator {
    public static <T> void generateId(List<T> objectList, T object) {
		int lastId;
		if (objectList.size() == 0) {
			DbStorage.setId(object, 1);
		} else {
			lastId = new Integer(DbStorage.getProperty(objectList.get(objectList.size()-1), "id"));
			DbStorage.setId(object, lastId + 1);
		}
	}	
}
