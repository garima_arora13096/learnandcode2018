/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITTdbOperations.FileUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

/**
 *
 * @author garima.arora
 */
public class FileStorage {
    public static <T> String getFilePath(T object) {
		return System.getProperty("user.dir") + "/ITTDBProject/" + object.getClass().getSimpleName() + ".json";
	}

	public static boolean isFileExist(String filePath) {
		return (Files.exists(Paths.get(filePath), LinkOption.NOFOLLOW_LINKS)) ? true : false;
	}

	public static File CreateFile(String filePath) {
		return new File(filePath);
	}

	public static boolean isFileEmpty(String filePath) throws IOException{
		boolean CheckEmpty = false;
		
		try(FileInputStream fileInputStream = new FileInputStream(filePath)) {			
			CheckEmpty = (fileInputStream.getChannel().size() == 0) ? true : false;
		}
		return CheckEmpty;
	}

}
