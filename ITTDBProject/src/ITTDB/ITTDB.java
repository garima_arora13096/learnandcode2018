/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITTDB;

import ITTdbOperations.Exceptions.ITTDBException;
import ITTdbOperations.DBUtil.DbStorage;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author garima.arora
 */
public class ITTDB  {
        public <T> void insert(T object) throws IOException, ITTDBException {
		DbStorage.insert(object);
	}
	public <T> List<T> search(T object, String searchKey, String searchValue)
			throws IOException, ITTDBException {
				return DbStorage.search(searchKey, searchValue, object);
			}
	public <T> int delete(T object, String deleteKey, String deleteValue)
			throws IOException, ITTDBException {
				return DbStorage.delete(object, deleteKey, deleteValue);
			}
	public <T> int update(T object, int id) throws IOException, ITTDBException {
		return DbStorage.update(object, id);
	}
}

