/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITTDB;

import ITTdbOperations.Exceptions.ITTDBException;
import java.io.IOException;
import java.util.List;



public interface ITTDB_Interface {
	public <T> void insert(T object) throws IOException, ITTDBException;
	public <T> List<T> search(T object, String searchKey, String searchValue)
			throws IOException, ITTDBException;
	public <T> int delete(T object, String deleteKey, String deleteValue)
			throws IOException, ITTDBException;
	public <T> int update(T object, int id) throws IOException, ITTDBException;

}