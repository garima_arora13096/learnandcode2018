package UserInterface;

public class StudentDetails {
	public int id;
	public String name;
	public String address;

	public StudentDetails() {

	}

	public StudentDetails(String name, String address) {
		this.name = name;
		this.address = address;
	}

	public StudentDetails(int id,String name, String address) {
		this.id = id;
		this.name = name;
		this.address = address;
	}

	  public int getId() { return id; }
	  
	  
	  public void setId(int id) { this.id = id; }
	  
	 
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Student [id = " + id + "Name = " + name + ", Address = " + address + "]";
	}
}
