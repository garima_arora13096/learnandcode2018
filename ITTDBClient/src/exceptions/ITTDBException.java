package exceptions;

public class ITTDBException extends Exception{
	String errorMessage;
	int errorCode;

	public ITTDBException(int errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public void printMessage() {
		System.out.println(errorCode + ":" + errorMessage);
	}


}
