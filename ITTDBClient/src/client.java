
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.json.simple.JSONObject;

import JsonConvertor.serialize;

public class client {
	Boolean ReadResponse=false;
	Scanner s;
	private Socket  socket   = null; 
	String message;

	public void input() throws UnknownHostException, IOException
	{
		s = new Scanner(System.in);
		System.out.print("Enter IP address: ");
		String ip = s.next();
		connect(ip,5000);
	}

	public void connect(String address,int port) throws UnknownHostException, IOException
	{
		try {
			socket = new Socket(address, port); 
			System.out.println("Connected"); 
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}

	public void sendSearchRequest(String RequestType, int id, String filename) throws IOException, IllegalArgumentException, IllegalAccessException
	{		
		OutputStream output= socket.getOutputStream();
		DataOutputStream dataOut=new DataOutputStream(output);
		JSONObject json = new JSONObject();
		JSONObject json_id = new JSONObject();
		json_id.put("id", id);
		json.put("type" , RequestType);
		json.put("Data",json_id );
		json.put("filename",filename);
		message = json.toJSONString();
		dataOut.writeUTF(message);
		System.out.println(message);
		receiveResponse();
	}
	
	public <T>void sendInsertRequest(String RequestType,T Object) throws IOException, IllegalArgumentException, IllegalAccessException
	{
		JSONObject serializeObject = serialize.serializeObjects(Object);
		OutputStream output= socket.getOutputStream();
		DataOutputStream dataOut=new DataOutputStream(output);		
		JSONObject json = new JSONObject();
		json.put("type" , RequestType);
		json.put("Data", serializeObject);
		json.put("filename",Object.getClass().getSimpleName());
		message = json.toJSONString();
		dataOut.writeUTF(message);
		System.out.println(message);
		receiveResponse();
	}
    
	public <T>void UpdateUserDetail(String RequestType, Object obj ,String filename) throws IOException, IllegalArgumentException, IllegalAccessException
	{
		System.out.println("inside UpdateUserDetail ");

		JSONObject serializeObject = serialize.serializeObjects(obj);
		OutputStream output= socket.getOutputStream();
		DataOutputStream dataOut=new DataOutputStream(output);		
		JSONObject json = new JSONObject();
		json.put("type" , RequestType);
		json.put("Data", serializeObject);
		json.put("filename",obj.getClass().getSimpleName());
		message = json.toJSONString();
		dataOut.writeUTF(message);
		System.out.println(message);
		receiveResponse();
	}
	
	public void receiveResponse() throws IOException {
		
		while(!ReadResponse) {
		InputStream input = socket.getInputStream();
		DataInputStream Datainput = new DataInputStream(input);
		String request = Datainput.readUTF();
		ReadResponse = true;
		System.out.println(request); 
		closeconnection();
		
		}
	}
	
	public void closeconnection() throws IOException
	{
		socket.close();
	}
}
