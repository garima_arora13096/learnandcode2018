package JsonConvertor;

import java.lang.reflect.Field;

import org.json.simple.JSONObject;

public class serialize {
	public static <T> JSONObject serializeObjects(T Object) throws IllegalArgumentException, IllegalAccessException
	{
		Field[] objectAttributes=getObjectAttributes(Object);
		JSONObject jsonObject=new JSONObject();
		for(Field property:objectAttributes){
			String propertyName=property.getName().toString();
			jsonObject.put(propertyName, property.get(Object));
		} 
		System.out.print(jsonObject);
		return jsonObject;
	}

	private static <T> Field[] getObjectAttributes(T Object) {	
		return Object.getClass().getFields();
	}
}
