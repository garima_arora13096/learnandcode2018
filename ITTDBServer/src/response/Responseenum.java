package response;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.json.simple.JSONObject;

public enum Responseenum {
	
		CREATED(201,"created"),
	    OK(200,"OK"), 
	    ERROR(500,"Error") ;
		
		private String phraseValue;
		private int StatusCode; 
	
		Responseenum(int Statuscode, String phraseValue) {
			this.StatusCode = Statuscode;
			this.phraseValue = phraseValue;
		
		}
		
		public String getPhraseValue() {
			return phraseValue;
		}

		public void setPhraseValue(String phraseValue) {
			this.phraseValue = phraseValue;
		}

		public int getStatusCode() {
			return StatusCode;
		}

		public void setStatusCode(int statusCode) {
			StatusCode = statusCode;
		}
		
		public static String createResponse(String StatusCode ,JSONObject updatedObject) throws IOException {

			
			JSONObject json = new JSONObject();	
			json.put("data",updatedObject);
			json.put("Status",StatusCode);
			String message = json.toJSONString();
			return message;
			//dataOut.writeUTF(message);
			
		}
		
		
}
