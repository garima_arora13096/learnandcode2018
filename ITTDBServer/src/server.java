import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import ITTDB.ITTDB;
import exceptions.ITTDBException;

public class server {

	private static Socket socket = null;
	private static ServerSocket server = null;

	private static void startServer(int port) throws IOException {
		server = new ServerSocket(port);
		System.out.println("Server started");
		System.out.println("Waiting for a client ...");
	}

	private static void Accept() throws Exception {
		while (true){
			socket = server.accept();
			System.out.println("Client accepted");
			String Response = receiveRequest();
			sendResponse(Response);
		}
		}

	private static void sendResponse(String Response) throws IOException {
		OutputStream output= socket.getOutputStream();
		DataOutputStream dataOut=new DataOutputStream(output);	
		dataOut.writeUTF(Response);
	}
	
	private static String receiveRequest() throws Exception {
		String Response = null;
		while (true) {
			ITTDB db=new ITTDB(); 

			InputStream input = socket.getInputStream();

			DataInputStream Datainput = new DataInputStream(input);

			String request = Datainput.readUTF();
			JSONParser parse = new JSONParser();
			JSONObject obj = (JSONObject) parse.parse(request);
			String value=(String) obj.get("type");
		
			JSONObject Data= (JSONObject) obj.get("Data");
			String filename=(String) obj.get("filename");

			switch (value) {
			case "insert":
				
				Response = db.insert(Data,filename);
				
				break;
			
			 case "delete":
				
				int Value1 = Integer.parseInt((Data.get("id").toString()));
			    db.delete("id", Value1, filename);
				 
				 
				break;

			case "search":
				int Value = Integer.parseInt((Data.get("id").toString()));
				db.search("id", Value, filename);
				
				break;
				
			case "update":
				System.out.println("operation is update");
				db.update(Data, filename);
				break;

			default:
				break;
				

			}
			return Response;
		}

	}
	
	
	public static void stopServer() throws IOException {
		socket.close();
	}

	public static void main(String args[]) throws Exception {
		startServer(5000);
		Accept();
	}
}
