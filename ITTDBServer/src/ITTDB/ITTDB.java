package ITTDB;

import exceptions.ITTDBException;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONObject;

import ITTDBOperations.DbStorage;

public class ITTDB {
	public <T> String insert(JSONObject data,String filename) throws Exception {
		return DbStorage.insert(data,filename);
	}


	public <T> List<T> search(String searchKey, int searchValue , String
			filename) throws IOException, ITTDBException, ClassNotFoundException { return
					DbStorage.search(searchKey,searchValue, filename); 
				
	}


	  public <T> int delete(String deleteKey, int deleteValue , String filename)
	  throws Exception { return DbStorage.delete(deleteKey,deleteValue , filename);
	  }
	 

	
	  public <T> int update(JSONObject data, String filename) throws Exception {
	  return DbStorage.update(data, filename); }
	 

}
