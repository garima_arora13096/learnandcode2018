package ITTDB;

import exceptions.ITTDBException;
import java.io.IOException;
import java.util.List;

public interface ITTDBInterface {
	public <T> void insert(T object) throws IOException, ITTDBException;
	public <T> List<T> search(String searchKey, String searchValue, String filename)
			throws IOException, ITTDBException;
	public <T> int delete(T object, String deleteKey, String deleteValue)
			throws IOException, ITTDBException;
	public <T> int update(T object, int id) throws IOException, ITTDBException;
}
