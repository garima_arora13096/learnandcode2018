/*
 * package ITTDBTest;
 * 
 * import exceptions.ITTDBException; import UserInterface.EmployeeDetails;
 * import UserInterface.StudentDetails; import java.io.IOException; import
 * java.util.List; import junit.framework.Assert; import org.junit.BeforeClass;
 * import org.junit.Test;
 * 
 * import ITTDB.ITTDB;
 * 
 * public class DBTester { static ITTDB db; static StudentDetails
 * student1,student2,student3; static EmployeeDetails
 * employee1,employee2,employee3;
 * 
 * @BeforeClass public static void testDeclareStudent() { db = new ITTDB();
 * student1 = new StudentDetails("Garima", "Udaipur"); }
 * 
 * @BeforeClass public static void testDeclareEmployee() { employee1 = new
 * EmployeeDetails("Garima", "Jr.Software Engineer"); }
 * 
 * @Test
 * 
 * public void testsaveStudent() throws IOException, ITTDBException {
 * db.insert(student1); }
 * 
 * @Test
 * 
 * public void testsaveEmployee() throws IOException, ITTDBException {
 * db.insert(employee1); }
 * 
 * @Test
 * 
 * public void testsearch() throws IOException, ITTDBException { EmployeeDetails
 * ExpectedDetails = new EmployeeDetails(); ExpectedDetails.setId(1);
 * ExpectedDetails.setName("Rajveer");
 * ExpectedDetails.setDesignation("Software Engineer"); List<EmployeeDetails>
 * employee = db.search(employee1, "id", "1");
 * Assert.assertEquals(employee.get(0).toString(),ExpectedDetails.toString()); }
 * 
 * @Test
 * 
 * public void TestDelete() throws IOException, ITTDBException { int
 * rowsAffected = db.delete(student1, "id", "1");
 * Assert.assertEquals(1,rowsAffected); }
 * 
 * @Test
 * 
 * public void testUpdate() throws IOException, ITTDBException {
 * employee1.setName("Rajveer"); employee1.setDesignation("Software Engineer");
 * int rowsUpdated = db.update(employee1, 1);
 * Assert.assertEquals(1,rowsUpdated); } }
 */