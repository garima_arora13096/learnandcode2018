package ITTDBOperations;
import exceptions.ITTDBException;
import response.Responseenum;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.crypto.SealedObject;
import javax.swing.text.html.HTMLEditorKit.Parser;
import javax.xml.ws.Response;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

public class DbStorage {


	public static <T> String insert(JSONObject Object ,String filename) throws Exception { 

		List<T> objectList = new ArrayList<T>(); 
		String filePath = FileStorage.getFilePath(filename);
		System.out.println(filePath);
		if (!FileStorage.isFileExist(filePath)) {
			FileStorage.CreateFile(filePath); 
		} 
		else if (!FileStorage.isFileEmpty(filePath)) { 
			objectList = (List<T>)ReadWriteObjectFromFile.ReadObjectsFromFile(filePath, Object.getClass()); 
		}
		int id=Idgenerator.generateId(objectList, Object); 
		JSONObject updatedObject=setId(Object,id);
		objectList =  AddObjectsInObjectList(objectList, updatedObject);
		ReadWriteObjectFromFile.WriteObjectsToFile(filePath, objectList);

		return Responseenum.createResponse(Responseenum.CREATED.toString() , updatedObject);
	}

	public static <T> List<T> AddObjectsInObjectList(List objectList, JSONObject object) {
		objectList.add(object);
		return objectList;
	}


	public static <T> JSONObject setId(JSONObject object, int id) { 
		Set keys=object.keySet();
		Iterator iterate=keys.iterator();

		while(iterate.hasNext()) {
			String key=iterate.next().toString();
			if(key.contains("id")) {
				object.put(key, id);
			}
		}

		return object;


	}

	public static <T> String getProperty(T object, String property) 
	{ String value = " "; try {
		value = object.getClass().getDeclaredField(property).get(object).toString(); 
	} catch(IllegalArgumentException | IllegalAccessException | NoSuchFieldException e)
	{ } return value; 
	}



	public static <T> int delete( String deleteKey, int deleteValue , String filename) 
			throws Exception { 
		int rowsAffected = 0;
		JSONObject Object = new JSONObject(); 
		String filePath =FileStorage.getFilePath(filename);
		List<T> objectList = (List<T>)ReadWriteObjectFromFile.ReadObjectsFromFile(filePath,Object.getClass());
		List<T> objectsToDelete = DbStorage.search(deleteKey, deleteValue, filename);
		rowsAffected = remove(objectsToDelete.get(0), objectList);
		if (rowsAffected > 0)
			ReadWriteObjectFromFile.WriteObjectsToFile(filePath,objectList); return
					rowsAffected; }


	public static <T> List<T> search(String searchKey, int searchValue, String filename) throws ITTDBException, IOException, ClassNotFoundException 
	{ 
		JSONObject Object = new JSONObject();
		String filePath = FileStorage.getFilePath(filename);
		List<T> objectList = (List<T>)ReadWriteObjectFromFile.ReadObjectsFromFile(filePath, Object.getClass());
		List searched_object =   (getSearchedObjects(searchKey, searchValue,objectList)); 
		return searched_object;
	}

	public static <T> List<T> getSearchedObjects(String searchKey, int searchValue, List<T> objectList) 
			throws ITTDBException 
	{ 

		List<T> searchedObjects = new ArrayList<>(); 

		for (T tempObject : objectList) { 
			JSONObject test= (JSONObject) tempObject;
			int search_Key = (Integer) test.get(searchKey);

			if(search_Key == searchValue){

				searchedObjects.add(tempObject); 
				System.out.println("search object");
				System.out.println(searchedObjects);
			} 
		} 
		return searchedObjects;

	}


	public static <T> int remove(T object, List<T> objectList) 
	{ 
		System.out.println(object);

		int rowsAffected= 0; 

		objectList.remove(object);
		rowsAffected +=1;
		return rowsAffected; 
	}


	public static <T> int update(JSONObject Object , String filename) throws Exception
	{ 
		int rowsAffected = 0; 
		String filePath = FileStorage.getFilePath(filename); 
		List<T> objectList = (List<T>)ReadWriteObjectFromFile.ReadObjectsFromFile(filePath, Object.getClass()); 
		rowsAffected = setUpdatedObjectInList(Object, objectList); 
		if (rowsAffected > 0)
			ReadWriteObjectFromFile.WriteObjectsToFile(filePath, objectList); 
		return rowsAffected; 
	}

	public static <T> int setUpdatedObjectInList(JSONObject object, List<T> objectList) 
	{
		int rowsAffected = 0; 
		System.out.println("updating object");
		System.out.println(object);
		for (int index = 0; index < objectList.size(); index++)
		{ T tempObject = objectList.get(index);	
		JSONObject test= (JSONObject) tempObject;
		int id = Integer.parseInt(object.get("id").toString());
		int test_id = Integer.parseInt(test.get("id").toString());
		if (test_id == id) {
			
			System.out.println("vbnm,./");
			System.out.println(index);
			System.out.println("kjkjhvcgh");
			objectList.set(index, (T) object);
			
//			objectList.remove(index);
//			AddObjectsInObjectList(objectList, object);
			rowsAffected += 1;
			break;
		}
		
	   } 
	 return rowsAffected;
	 }







}
