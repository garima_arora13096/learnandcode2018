package ITTDBOperations;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
public class FileStorage {

	public static String getFilePath(String filename) {
//		return System.getProperty("user.dir") + "\\DB-Store\\"+ filename + ".json";
		String userHome = System.getProperty("user.home");
		String storageFolder = userHome + "\\DB-store\\";
		File dir = new File(storageFolder);
		dir.mkdirs();
		
		return (storageFolder + filename + ".json");
	}

	public static boolean isFileExist(String filePath) {
		return (Files.exists(Paths.get(filePath), LinkOption.NOFOLLOW_LINKS)) ? true : false;
	}

	public static void CreateFile(String filePath) {
		
		try {
			new File(filePath).createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean isFileEmpty(String filePath) throws IOException{
		boolean CheckEmpty = false;

		try(FileInputStream fileInputStream = new FileInputStream(filePath)) {			
			CheckEmpty = (fileInputStream.getChannel().size() == 0) ? true : false;
		}
		return CheckEmpty;
	}

}
